import 'dart:io';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:my_instagram/utils/routes.dart';
import 'package:my_instagram/ui/main/main_screen.dart';
import 'package:my_instagram/ui/login/login_screen.dart';
import 'package:my_instagram/ui/splash/splash_screen.dart';

/// Charles (HTTP request inspector)
class ProxiedHttpOverrides extends HttpOverrides {
  final String _port;
  final String _host;

  ProxiedHttpOverrides(this._host, this._port);

  @override
  HttpClient createHttpClient(SecurityContext? context) {
    HttpClient client = super.createHttpClient(context);
    // client.badCertificateCallback =
    //     (X509Certificate cert, String host, int port) => true;
    client.findProxy = (uri) {
      return "PROXY $_host:$_port;";
    };
    return client;
  }
}

void main() {
  // HttpOverrides.global = ProxiedHttpOverrides('192.168.0.111', '8888');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: splashRoute,
      routes: <String, WidgetBuilder>{
        splashRoute: (context) => const SplashScreen(),
        loginRoute: (context) => const LoginScreen(),
        mainRoute: (context) => const MainScreen(),
      }
    );
  }
}