import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_instagram/controller/api_controller.dart';
import 'package:my_instagram/ui/custom_widgets/custom_inkwell.dart';
import 'package:my_instagram/ui/custom_widgets/custom_story_item.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ApiController _apiController = Get.put(ApiController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 100),
        CustomInkWell(
          onTap: () {
            _apiController.fetchUserInfo();
          },
          child: Text('Click Me'),
        ),
        const SizedBox(height: 100),
        Obx(() {
          return _apiController.userInfo.isEmpty
              ? Container()
              : Column(
                  children: [
                    Text(_apiController.userInfo['name'] ?? ''),
                  ],
                );
        }),
        // Obx(() {
        //   return Text('Counter, ${_apiController.counter}');
        // }),
        const SizedBox(height: 10),
        SizedBox(
          height: 62,
          child: ListView.builder(
            itemCount: 10,
            padding: EdgeInsets.zero,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return CustomStoryItem();
            },
          ),
        ),
      ],
    );
  }
}
