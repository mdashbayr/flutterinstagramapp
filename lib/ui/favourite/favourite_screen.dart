import 'package:flutter/material.dart';

class FavouriteScreen extends StatefulWidget {
  const FavouriteScreen({Key? key}) : super(key: key);

  @override
  State<FavouriteScreen> createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> with AutomaticKeepAliveClientMixin<FavouriteScreen> {
  @override
  void initState() {
    print("Fav screen inited");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.black38,
    );
  }

  @override
  bool get wantKeepAlive => true;
}
