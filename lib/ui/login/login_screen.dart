import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:my_instagram/controller/api_controller.dart';
import 'package:my_instagram/ui/custom_widgets/custom_inkwell.dart';
import 'package:my_instagram/utils/routes.dart';
import 'package:my_instagram/utils/sp_manager.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final ValueNotifier<String> _name = ValueNotifier('Saraa');
  final ApiController _apiController = Get.put(ApiController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomInkWell(
            onTap: _loginEventHandler,
            child: Container(
              width: double.infinity,
              alignment: Alignment.center,
              child: const Text('Login with facebook'),
            ),
          ),
          const SizedBox(height: 20),
          CustomInkWell(
            onTap: () {
              print("clicked");
              // _apiController.counter.value++;
              _name.value = 'Maidar';
            },
            child: Container(
              height: 40,
              width: double.infinity,
              alignment: Alignment.center,
              child: const Text('Increment'),
            ),
          ),
          const SizedBox(height: 20),
          Obx(() {
            return Text('Counter, ${_apiController.counter}');
          }),
          const SizedBox(height: 20),
          ValueListenableBuilder(
            valueListenable: _name,
            builder: (_, String value, __) {
              return Text(value);
            },
          ),
        ],
      ),
    );
  }

  void _loginEventHandler() async {
    // final LoginResult result = await FacebookAuth.instance.login();
    // if (result.status == LoginStatus.success) {
    //   final AccessToken accessToken = result.accessToken!;
    //   print("accessToken: $accessToken");
    // } else {
    //   print(result.status);
    //   print(result.message);
    // }
    print("Clicked");
    // CustomDialog.showLoader();
    // Get.toNamed(homeRoute);
    // Get.back();
    // Get.offAllNamed(secondRoute);
    var url = Uri.parse('http://192.168.0.119:8000/api/v1/users/login');
    try {
      var response = await http.post(url,
          body: jsonEncode({'email': 'bat@gmail.com', 'password': '1234'}),
          headers: {"Content-Type": 'application/json'});
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      var responseData = Map<String, dynamic>.from(jsonDecode(response.body));

      if (responseData['success'] == true) {
        String accessToken = responseData['token'] ?? '';

        SpManager sharedPreference = SpManager();
        await sharedPreference.init();

        sharedPreference.saveAccessToken(accessToken);
        Get.offAllNamed(mainRoute);
      }
    } catch (e) {
      print("exception: ${e.toString()}");
    }
  }
}
