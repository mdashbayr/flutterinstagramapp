import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:my_instagram/utils/sp_manager.dart';

class ApiController extends GetxController {
  RxInt counter = RxInt(0);
  RxMap<String, dynamic> userInfo = RxMap();

  void fetchUserInfo() async {
    SpManager sharedPreference = SpManager();
    await sharedPreference.init();
    String accessToken = await sharedPreference.getAccessToken();

    var url = Uri.parse('http://192.168.0.119:8000/api/v1/users');
    try {
      var response = await http.get(url,
          headers: {
            "Content-Type": 'application/json',
            'Authorization': 'Bearer $accessToken',
          });
      var responseData = Map<String, dynamic>.from(jsonDecode(response.body));

      if (responseData['success'] == true) {
        print("responseData: $responseData");
        userInfo.value = responseData['user'][0];
      }
    } catch (e) {
      print("exception: ${e.toString()}");
    }
  }
}
